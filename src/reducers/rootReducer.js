import recipeReducer from './recipeReducer';
import {combineReducers} from 'redux';

//Combine all the sub reducers
const rootReducer = combineReducers({
    recipes:recipeReducer
})

export default rootReducer