const initState = {
    recipeList: [
        {
            id: 1,
            name: 'Crispy Fried Corn',
            ingredientName: 'Sweet corn',
            ingredientQuantity: '1 cup',
            unit: 'Cup',
            steps: 'Take a vessel, fill with 2 cups water.Turn on flame. Let the water boil.Put the sweet corn to boil in water for 5 to 6 minutes',
            url: 'complex.jpg',
        }, {
            id: 2,
            name: 'Tomato Juice Canning',
            ingredientName: 'Tomatos',
            ingredientQuantity: '5',
            unit: 'Peice',
            steps: 'Crush all the tomatos',
            url: 'burgers.jpg',
        }, {
            id: 3,
            name: 'Smoked mackerel',
            ingredientName: 'new potatoes , halved',
            ingredientQuantity: '250g ',
            unit: 'Gm',
            steps: 'Make this mackerel and leek hash in just 30 minutes',
            url: 'breakfast.jpg',
        }
    ]
}

const recipeReducer = (state = initState, action) => {
    switch (action.type) {
        case 'ADD_RECIPE':
            return {
                ...state,
                recipeList: [
                    ...state.recipeList,
                    action.payload
                ]
            }
        case 'REMOVE_RECIPE':
            return {
                ...state,
                recipeList: action.payload
            }
        default:
            return state
    }
}

export default recipeReducer;