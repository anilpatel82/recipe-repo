import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto auto 15px auto',
    maxWidth: 500,
  },
  image: {
    width: 128,
    height: 128,
  },
  [theme.breakpoints.down('lg')]: {
    image:{
      width: "100%",
    },
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  }
}));

const RecipeItem = (props) => {
    const classes = useStyles();
    const recipeList = useSelector(state => state.recipes.recipeList)
    const dispatch = useDispatch();

    const removeTodoItem = (recipeId) => {
        let newRecipeList = recipeList.filter(item => item.id !== recipeId);
        dispatch({type: 'REMOVE_RECIPE', payload: newRecipeList})
    }

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid container spacing={2}>
            <Grid item>
              <ButtonBase className={classes.image}>
                <img className={classes.img} alt="complex" src={props.item.url} />
              </ButtonBase>
            </Grid>
            <Grid item xs={12} sm container>
              <Grid item xs container direction="column" spacing={2}>
                <Grid item xs>
                  <Typography variant="h6" gutterBottom >
                    {props.item.name}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                  <b>Ingredient Name:</b> {props.item.ingredientName}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                  <b>Qty:</b> {props.item.ingredientQuantity}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                  <b>Unit:</b> {props.item.unit}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                  <b>Steps To Cook</b>: {props.item.steps}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant="body2" style={{ cursor: "pointer" }}>
                    <Button
                    variant="outlined"
                    color="secondary"
                      onClick={() => {
                        removeTodoItem(props.item.id);
                      }}
                      className="secondary-content"
                    > Remove
                    </Button>
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
}

export default RecipeItem;