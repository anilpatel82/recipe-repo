import React from 'react';
import RecipeList from './views/RecipeList';
import Header from './views/Header';

function App() {

    return (
        <div className="App">
            <Header />  
            <main className="container">
                <RecipeList/>
            </main>
        </div>
    );
}

export default App;
