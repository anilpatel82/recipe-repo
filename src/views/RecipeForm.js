import React, { useState } from 'react';
import { Button,  FormControl, InputLabel, FormHelperText } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';

const useStyles = makeStyles((theme) => ({
  formContainer: {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      width: "50%",
    },
  },
  form: {
    width: "100%",
  },
  mr10:{
    marginRight: "10px"
  }
}));

export default function RecipeForm (props) {
  const classes = useStyles();
  const { onSubmit } = props

  const [formVal, setFormVal] = useState({name:"",ingredientName:"",ingredientQuantity:"",unit:"",steps:"",url:""})
  const [validationsErr, setValidationsErr] = useState("");

  const handleChange = (event) => {
    setFormVal({...formVal,[event.target.name]:event.target.value})
  }

  const handleSubmit = () => {
    console.log("handleSubmit",formVal)
    if(formValidation()){
      onSubmit(formVal)
      resetForm()
    }
  }

  const formValidation = (val) => {
    let errors = {};
    let formIsValid = true;
    if(!formVal["name"]) {
      formIsValid = false;
      errors["name"] = "Name can not be empty";
    } else {
      formIsValid = true;
    }

    setValidationsErr({ errors: errors });
    return formIsValid;
  }

  const resetForm = () => {
    setFormVal({name:"",ingredientName:"",ingredientQuantity:"",unit:"",steps:"",url:""})
    setValidationsErr({ errors: {} });
  }

  return (
    <form noValidate autoComplete="off">
      <FormControl fullWidth>
        <InputLabel htmlFor="name">Name</InputLabel>
        <Input
          disableUnderline={true}
          id="name"
          error={true}
          value={formVal.name}
          aria-describedby="name-helper-text"
          name="name"
          onChange={handleChange}
        />
        {validationsErr && validationsErr.errors.name != "" && (
          <FormHelperText error={true} id="name-helper-text">
            {validationsErr.errors.name}
          </FormHelperText>
        )}
      </FormControl>

      <FormControl fullWidth>
        <InputLabel htmlFor="ingredientName">Ingredient Name</InputLabel>
        <Input
          disableUnderline={true}
          id="ingredientName"
          error={true}
          value={formVal.ingredientName}
          aria-describedby="ingredientName-helper-text"
          name="ingredientName"
          onChange={handleChange}
        />
        {validationsErr && validationsErr.errors.ingredientName != "" && (
          <FormHelperText error={true} id="ingredientName-helper-text">
            {validationsErr.errors.ingredientName}
          </FormHelperText>
        )}
      </FormControl>

      <FormControl fullWidth>
        <InputLabel htmlFor="ingredientQuantity">
          Ingredient Quantity
        </InputLabel>
        <Input
          disableUnderline={true}
          id="ingredientQuantity"
          error={true}
          value={formVal.ingredientQuantity}
          aria-describedby="ingredientQuantity-helper-text"
          name="ingredientQuantity"
          onChange={handleChange}
        />
      </FormControl>

      <FormControl fullWidth>
        <InputLabel htmlFor="unit">Unit</InputLabel>
        <Input
          disableUnderline={true}
          id="unit"
          error={true}
          value={formVal.unit}
          aria-describedby="unit-helper-text"
          name="unit"
          onChange={handleChange}
        />
      </FormControl>

      <FormControl fullWidth>
        <InputLabel htmlFor="steps">Steps To Cook</InputLabel>
        <Input
          disableUnderline={true}
          id="steps"
          error={true}
          value={formVal.steps}
          aria-describedby="steps-helper-text"
          name="steps"
          onChange={handleChange}
        />
      </FormControl>

      <FormControl fullWidth>
        <InputLabel htmlFor="steps">Image URL</InputLabel>
        <Input
          disableUnderline={true}
          id="url"
          error={true}
          value={formVal.url}
          aria-describedby="url-helper-text"
          name="url"
          onChange={handleChange}
        />
      </FormControl>

      <Button
        type="button"
        className={classes.mr10}
        variant="contained"
        color="primary"
        onClick={() => {
          handleSubmit();
        }}
      >
        Save
      </Button>
      <Button
        variant="contained"
        color="secondary"
        type="button"
        onClick={() => {
          resetForm();
        }}
      >
        Reset
      </Button>
    </form>
  );
};