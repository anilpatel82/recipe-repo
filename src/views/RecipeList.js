import React from 'react';
import { useSelector,useDispatch } from 'react-redux';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import RecipeItem from '../components/RecipeItem';
import RecipeForm from './RecipeForm';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
  table: {
    minWidth: 650,
  }
}));

const styles = (theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const RecipeList = () => {
  const [open, setOpen] = React.useState(false);

  const classes = useStyles();

  const recipeList = useSelector(state => state.recipes.recipeList);

  const dispatch = useDispatch();

  const handleClickOpen = () => {
    setOpen(true);
  };
  
  const handleClose = () => {
    setOpen(false);
  };
  
  const addNewRecipe = (values)=>{
    dispatch({type:'ADD_RECIPE',payload:values});
    handleClose()
  }
  

    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
              <div>
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={handleClickOpen}
                >
                  Add Recipe
                </Button>
                <Dialog
                  onClose={handleClose}
                  aria-labelledby="customized-dialog-title"
                  open={open}
                >
                  <DialogTitle
                    id="customized-dialog-title"
                    onClose={handleClose}
                  >
                    Add Recipe
                  </DialogTitle>
                  <DialogContent dividers>
                    <RecipeForm onSubmit={addNewRecipe} />
                  </DialogContent>
                </Dialog>
              </div>
              {recipeList &&
                recipeList.length > 0 &&
                recipeList.map((row) => <RecipeItem key={row.id} item={row} />)}
          </Grid>
        </Grid>
      </div>
    );
}
 
export default RecipeList;